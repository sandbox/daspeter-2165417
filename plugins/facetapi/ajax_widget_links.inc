<?php

/**
 * @file
 * The facetapi_links with ajax support.
 */

/**
 * Widget that renders facets as a list of clickable links with ajax support.
 */
class FacetapiAjaxWidgetLinks extends FacetapiWidgetLinks {
  /**
   * Overrides FacetapiWidgetCheckboxLinks::init().
   */
  public function init() {
    parent::init();
    ajax_facets_add_widget_ajax_js($this->settings);
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Add the class for the ajax handling.
   */
  public function getItemClasses() {
    return array_merge(array('facetapi-ajax'), parent::getItemClasses());
  }
}
