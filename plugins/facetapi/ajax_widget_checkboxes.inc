<?php

/**
 * @file
 * The facetapi_checkbox_links with ajax support.
 */

/**
 * Widget that renders facets as a list of clickable links with ajax support.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup.
 */
class FacetapiAjaxWidgetCheckboxes extends FacetapiWidgetCheckboxLinks {

  /**
   * Overrides FacetapiWidgetCheckboxLinks::init().
   */
  public function init() {
    parent::init();
    ajax_facets_add_widget_ajax_js($this->settings);
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Add the class for the ajax handling.
   */
  public function getItemClasses() {
    return array('facetapi-ajax', 'facetapi-ajax-checkbox');
  }

}
