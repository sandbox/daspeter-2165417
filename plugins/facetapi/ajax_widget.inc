<?php

/**
 * @file
 * Base ajax_facets widget.
 */

/**
 * Widget base class for other ajax widgets to extend.
 */
class FacetapiAjaxWidget extends FacetapiWidgetCheckboxLinks {

  /**
   * Overrides FacetapiWidgetCheckboxLinks::init().
   *
   * Adds additional JavaScript settings and CSS.
   */
  public function init() {
    parent::init();
    // Register the widget in the ajax handling.
    ajax_facets_add_widget_ajax_js($this->settings);
  }

  /**
   * Overrides FacetapiWidgetLinks::getItemClasses().
   *
   * Add the class for the ajax handling.
   */
  public function getItemClasses() {
    return array('facetapi-ajax');
  }
}
