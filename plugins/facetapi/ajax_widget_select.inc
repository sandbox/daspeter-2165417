<?php

/**
 * @file
 * Provides a select input field to select facets.
 */

/**
 * Widget that renders facets as a select input field.
 */
class FacetapiAjaxWidgetSelect extends FacetapiAjaxWidget {

  /**
   * Build the options for the select box.
   *
   * @param array $build
   *   The items in the facet's render array being transformed.
   *
   * @param string $active_path
   *   Stores the active value if one is found.
   *
   * @return array
   *   The options list for the select element.
   */
  public function buildOptions($build, &$active_path = NULL) {
    // Recursion handling.
    static $depth;
    if (!isset($depth)) {
      $depth = 0;
    }
    $settings = $this->settings->settings;
    $options = array();
    foreach ($build as $value => $item) {
      $path = url($item['#path'], array(
        'query' => $item['#query'],
      ));

      // Sanitizes the text.
      $option_label = check_plain($item['#markup']);
      // Adds count to the text if one was passed.
      if (isset($item['#count'])) {
        $option_label .= ' ' . theme('facetapi_count', array('count' => $item['#count']));
      }

      $options[$path] = $option_label;
      // If the item is active or the "show_expanded" setting is selected,
      // show this item as expanded so we see its children.
      if ($item['#active'] || !empty($settings['show_expanded'])) {
        if (!empty($item['#item_children'])) {
          $depth++;
          $options[check_plain($item['#markup'])] = $this->buildOptions($item['#item_children'], $active_path);
          $depth--;
        }
        $active_path = $path;

      }
    }
    // No-Filter option.
    if ($depth == 0) {
      $options = array(url($item['#path']) => t('All')) + $options;
    }
    return $options;
  }

  /**
   * Implements FacetapiWidget::execute().
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];

    $active_path = NULL;
    $options = $this->buildOptions($element, $active_path);

    // @TODO Add option to toggle the title.
    $select = array(
      '#type' => 'select',
      '#title' => '',
      '#options' => $options,
      '#value' => $active_path,
      '#id' => 'ajax-facets-selectbox-' . str_replace(array('_', ' ', ':'), '-', $this->settings->facet),
      '#name' => urlencode($this->settings->facet),
      '#attributes' => array(
        'data-facet' => $this->settings->facet,
      ),
    );

    $element = array(
      '#markup' => '<div class="facet-wrapper-selectbox ' . $this->build['#attributes']['id'] . '">'
      . render($select)
      . '</div>',
    );
  }
}
