(function ($) {

  /**
   * Class containing functionality for Facet API.
   */
  Drupal.ajax_facets = {};

  /**
   * Function to refresh a search api view based on a facet path.
   * @param facet_path
   *   The facet path to filter with.
   * @returns {boolean}
   */
  Drupal.ajax_facets.refreshView = function(facet_path) {
    // Change the ajax URL of the ajax call to inject the facet setting.
    // Retrieve the path to use for views' ajax.
    var ajax_path = Drupal.settings.views.ajax_path;

    // If there are multiple views this might've ended up showing up multiple times.
    if (ajax_path.constructor.toString().indexOf("Array") != -1) {
      ajax_path = ajax_path[0];
    }

    // Check if there are any GET parameters to send to views.
    var queryString = facet_path.replace(/^.+?(\?|$)/, '?') || '';
    if (queryString !== '') {
      // Remove the question mark and Drupal path component if any.
      var queryString = queryString.slice(1).replace(/q=[^&]+&?|&?render=[^&]+/, '');
      if (queryString !== '') {
        // If there is a '?' in ajax_path, clean url are on and & should be used to add parameters.
        queryString = ((/\?/.test(ajax_path)) ? '&' : '?') + queryString;
      }
    }

    var views_dom_id = 'views_dom_id:' + Drupal.settings.ajax_facets.view;

    // Build data to pass by POST.
    var viewsData = $.extend(
      {},
      Drupal.views.instances[views_dom_id].settings,
      Drupal.Views.parseQueryString(ajax_path + queryString),
      Drupal.Views.parseViewArgs(ajax_path + queryString, ajax_path)
    );
    Drupal.views.instances[views_dom_id].refreshViewAjax.options.data = viewsData;

    // Ensure we load from the start.
    Drupal.views.instances[views_dom_id].refreshViewAjax.options.data.page = 0;

    // Remove any facet parameters from the ajax url to allow pre-set facets
    // to be toggled by ajax.
    Drupal.views.instances[views_dom_id].refreshViewAjax.url = Drupal.views.instances[views_dom_id].refreshViewAjax.url.replace(/&{0,1}f\[\d+\]=.*?(&|$)/, '$1')
    Drupal.views.instances[views_dom_id].refreshViewAjax.element_settings.url = Drupal.views.instances[views_dom_id].refreshViewAjax.element_settings.url.replace(/&{0,1}f\[\d+\]=.*?(&|$)/, '$1')
    Drupal.views.instances[views_dom_id].refreshViewAjax.options.url = Drupal.views.instances[views_dom_id].refreshViewAjax.options.url.replace(/&{0,1}f\[\d+\]=.*?(&|$)/, '$1')

    // If possible adjust the window url.
    if (typeof window.history.pushState != 'undefined') {
      window.history.pushState({}, document.title, facet_path);
    }

    // Trigger a refresh of the view.
    $(Drupal.views.instances[views_dom_id].refreshViewAjax.element).trigger('RefreshView');
    return false;
  };

  /**
   * Initialize checkboxes with ajax support - based on Drupal.behaviors.facetapi.attach.
   */
  Drupal.ajax_facets.init_ajax_checkboxes = function(context, settings) {
    // If this is an ajax update ensure we use the parent of the context to
    // rebuild the behaviour.
    if ($(context).hasClass('facetapi-facetapi-ajax-checkboxes')) {
      context = context.parent();
    }

    if (settings.facetapi) {
      // Initialize checkboxes.
      for (var index in settings.facetapi.facets) {
        if (null != settings.facetapi.facets[index].makeCheckboxes) {
          // Find all checkbox facet links and give them a checkbox.
          $('#' + settings.facetapi.facets[index].id + ' a.facetapi-ajax-checkbox', context)
            .each(Drupal.facetapi.makeCheckbox)
        }
      }
      // Adjust the behaviour to support ajax.
      $('.facetapi-facetapi-ajax-checkboxes .facetapi-checkbox', context).each(function(){
        // Remove original handling and add our own ajax behaviour.
        $(this)
          .unbind('click') // Since facetapi doesn't provide a namespace remove all.
          .bind('click.ajax_facets', function() {
            var link = $(this).parent().find('a.facetapi-ajax');
            // Check if we can use the views handling.
            if (link && typeof(Drupal.settings.ajax_facets.view) != 'undefined') {
              // Decoupling the call to refresh view to ensure the checkbox
              // reacts first.
              window.setTimeout(function() {
                Drupal.ajax_facets.refreshView(link[0].href);
              }, 1);
            }
          });
      });
    }
  };

  /**
   * Some ajax commands can falsify the context to just one element even if
   * there are several elements. Try our best to fix that.
   */
  Drupal.ajax_facets.get_context = function(context) {
    context = $(context);
    var context_selector = '';
    context.each(function(i, elem){
      context_selector += (context_selector.lenght) ? ', ' : '';
      if ($(elem).prop('tagName')) {
        context_selector += $(elem).prop('tagName');
      }
      if ($(elem).attr('id')) {
        context_selector += '#' + $(elem).attr('id');
      }
      if ($(elem).attr('class')) {
        context_selector += '.' + $(elem).attr('class').replace(/ /g, '.');
      }
    });
    if (context_selector) {
      context = $(context_selector);
    }
    return context;
  };

  Drupal.behaviors.ajax_facets = {
    attach: function(context, settings) {
      context = Drupal.ajax_facets.get_context(context);
      $('a.facetapi-ajax, .facetapi-ajax a', context).each(function(){
        $(this).bind('click.ajax_facets', function() {
          // Check if we can use the views handling.
          if (typeof(Drupal.settings.ajax_facets.view) != 'undefined') {
            return Drupal.ajax_facets.refreshView(this.href);
          }
        });
      });
      // Handle checkboxes widget links.
      Drupal.ajax_facets.init_ajax_checkboxes(context, settings);

      // Handle select boxes.
      $('.facet-wrapper-selectbox .form-select', context).each(function(){
        $(this).bind('change.ajax_facets', function() {
          // Check if we can use the views handling.
          if (typeof(Drupal.settings.ajax_facets.view) != 'undefined') {
            return Drupal.ajax_facets.refreshView(this.value);
          }
        });
      });

    },
    detach: function(context, settings) {
    }
  }

  Drupal.behaviors.ajax_current_search_item = {
    attach: function(context, settings) {
      context = Drupal.ajax_facets.get_context(context);
      $('.current-search-item a', context).each(function(){
        $(this).bind('click.ajax_facets', function() {
          // Check if we can use the views handling.
          if (typeof(Drupal.settings.ajax_facets.view) != 'undefined') {
            return Drupal.ajax_facets.refreshView(this.href);
          }
        });
      });
    },
    detach: function(context, settings) {
    }
  }
})(jQuery);
